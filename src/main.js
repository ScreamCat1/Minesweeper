$(document).ready(function () {
  set(5, 5);

  $('#go-btn').click(function () {
    startGame();
  });


  $('.close-modal').click(function () {
    $('.modal-bg__success, .modal-bg__lose, .modal-bg__check')
      .removeClass('modal-bg__show');
    $('#bomb-quantity').val('');
    $('#table-quantity').val('');
    $('#container').html('');

  });

  $("#container").contextmenu(function (e) {
    $(e.target).toggleClass('flag');
    return false
  });
});

