function Field(num) {
  let res = [];
  for (let i = 0; i < num; i++) {
    let temp = [];
    for (let j = 0; j < num; j++)
      temp[j] = 0;
    res[i] = temp;
  }
  return res;
}

function addNums(field) {
  for (let i = 0; i < field.length; i++) {
    for (let j = 0; j < field[i].length; j++) {
      if (field[i][j] == -1) {
        setAround(field, i, j);
      }
    }
  }
}

function renderField(field, $root) {
  $root.html('');
  field.map((line, row) => {
    line.map((item, col) => {
      let div = $('<div></div>')
        .addClass('element');
      switch (item) {
        case -1 :
          div.addClass('bomb');
          break;
        default :
          break;
      }
      div.data('sibling', item)
        .attr('data-row', row)
        .attr('data-col', col)
        .appendTo($root);
    })
  });
  let sheet = document.styleSheets[0];
  sheet.addRule(".element", "width:" + 100 / field.length + "%", sheet.length);
  sheet.addRule(".element", "height:" + 100 / field.length + "%", sheet.length);
}

function setAround(field, row, col) {
  for (let i = -1; i < 2; i++) {
    for (let j = -1; j < 2; j++) {
      let ii = +row + i;
      let jj = +col + j;
      let fieldLength = field.length;
      if (ii < 0 || jj < 0 || ii > fieldLength - 1 || jj > fieldLength - 1 || field[ii][jj] === -1) {
      } else {
        field[ii][jj]++;
      }
    }
  }
}

function addBombs(field, num) {
  let count = 0;
  let fieldLength = field.length - 1;
  while (count !== num) {
    let posI = randomInteger(fieldLength);
    let posJ = randomInteger(fieldLength);
    if (field[posI][posJ] === 0) {
      field[posI][posJ] = -1;
      count++;
    }
  }
}

function randomInteger(max) {
  let rand = 0 - 0.5 + Math.random() * (max - 0 + 1);
  rand = Math.round(rand);
  return rand;
}

function startGame() {
  let bombCount = Number($('#bomb-quantity').val());
  let tableSize = Number($('#table-quantity').val());
  if (bombCount && tableSize && tableSize * tableSize >= bombCount) {
    set(tableSize, bombCount);
  } else {
    $('.modal-bg__check').addClass('modal-bg__show');
  }
}

function set(tableSize, bombCount) {
  let field = new Field(tableSize);
  addBombs(field, (bombCount));
  addNums(field);
  renderField(field, $('#container'));
  $('#container div').click(function (e) {
    elementClick(e, tableSize, bombCount);
  });
}

function elementClick(e, tableSize, bombCount) {
  let $item = $(e.target);
  let row = $item.attr('data-row');
  let col = $item.attr('data-col');


  visitNext(row, col);
  let visited = $('.visited').not('.bomb').length;
  if (visited === tableSize * tableSize - bombCount) {
    $('.modal-bg__success').addClass('modal-bg__show');
    setScore($('.score-board--elem__win-count'));
  }

  function visitNext(row, col) {
    let $item;

    row = +row;
    col = +col;
    $item = $(`[data-row=${row}][data-col=${col}]`);

    if (!$item.length || $item.hasClass('visited')) {
      return;
    }

    let dataItem = $item.data('sibling');
    let classItem = '';
    $item.addClass('visited');
    switch (dataItem) {
      case 0:
        classItem = 'empty';
        $item.addClass(classItem);
        break;
      case -1:
        classItem = 'bomb';
        $('.bomb').addClass('active');
        $('.modal-bg__lose').addClass('modal-bg__show');
        setScore($('.score-board--elem__lose-count'));
        $item.addClass(classItem);
        return;
        break;
      default:
        $item.html(dataItem);
        return;
        break;
    }

    for (let i = -1; i < 2; i++) {
      for (let j = -1; j < 2; j++) {
        visitNext(row + i, col + j);
      }
    }

  }
}

function setScore($elem) {
  let score = Number($elem.html()) + 1;
  $elem.html(score);
}
